#include "pch.h"
#include "OrbiterMeshTriangle.h"

using namespace std;

void OrbiterMeshTriangle::Flip(PointF::Axis axis)
{
	auto t = p1_;
	p1_ = p3_;
	p3_ = t;
}

void OrbiterMeshTriangle::saveToFile(ofstream & stream)
{
	stream << p1_ << " " << p2_ << " " << p3_ << endl;
}

OrbiterMeshTriangle::OrbiterMeshTriangle(vector<OrbiterMeshPoint> points, int p1, int p2, int p3)
	: p1_(p1)
	, p2_(p2)
	, p3_(p3)
{
	auto v2 = points[p2].coord() - points[p1].coord();
	auto v3 = points[p3].coord() - points[p1].coord();
	auto vN = points[p1].normal() - points[p1].coord();

	if ((vN * (v3 % v2)) >= 0) {
		auto t = p2_;
		p2_ = p3_;
		p3_ = t;
	}
}

OrbiterMeshTriangle::~OrbiterMeshTriangle()
{
}
