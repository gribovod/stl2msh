#include "pch.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "OrbiterMeshPoint.h"

using namespace std;

void OrbiterMeshPoint::Flip(PointF::Axis axis)
{
	switch (axis)
	{
	case PointF::Axis::X: {
		coord_.x *= -1;
		normal_.x *= -1;
		break;
	}
	case PointF::Axis::Y: {
		coord_.y *= -1;
		normal_.y *= -1;
		break;
	}
	case PointF::Axis::Z: {
		coord_.z *= -1;
		normal_.z *= -1;
		break;
	}
	default:
		break;
	}
}

void OrbiterMeshPoint::Scale(float scale)
{
	auto nN = normal_ - coord_;
	coord_ *= scale;
	normal_ = nN + coord_;
}

void OrbiterMeshPoint::Rotate(PointF::Axis axis, float cosA, float sinA)
{
	switch (axis)
	{
	case PointF::Axis::X: {
		auto oldZ = coord_.z;
		auto oldY = coord_.y;
		coord_.z = oldZ * cosA - oldY * sinA;
		coord_.y = oldZ * sinA + oldY * cosA;
		oldZ = normal_.z;
		oldY = normal_.y;
		normal_.z = oldZ * cosA - oldY * sinA;
		normal_.y = oldZ * sinA + oldY * cosA;
		break; }
	case PointF::Axis::Y:{
		auto oldX = coord_.x;
		auto oldZ = coord_.z;
		coord_.x = oldX * cosA - oldZ * sinA;
		coord_.z = oldX * sinA + oldZ * cosA;
		oldX = normal_.x;
		oldZ = normal_.z;
		normal_.x = oldX * cosA - oldZ * sinA;
		normal_.z = oldX * sinA + oldZ * cosA;
		break;
	}
	case PointF::Axis::Z:{
		auto oldX = coord_.x;
		auto oldY = coord_.y;
		coord_.x = oldX * cosA - oldY * sinA;
		coord_.y = oldX * sinA + oldY * cosA;
		oldX = normal_.x;
		oldY = normal_.y;
		normal_.x = oldX * cosA - oldY * sinA;
		normal_.y = oldX * sinA + oldY * cosA;
		break;
}
	default:
		break;
	}
}

void OrbiterMeshPoint::saveToFile(ofstream & stream, bool noNormal)
{
	if (noNormal) {
		stream << coord_.x << " " << coord_.y << " " << coord_.z << " 0.992 0.992" << endl;
	}
	else {
		stream << coord_.x << " " << coord_.y << " " << coord_.z << " " << normal_.x << " " << normal_.y << " " << normal_.z << " 0.992 0.992" << endl;
	}
}

OrbiterMeshPoint::OrbiterMeshPoint(PointF coord, PointF normal)
	: coord_(coord)
	, normal_(coord + normal)
{
}


OrbiterMeshPoint::~OrbiterMeshPoint()
{
}
