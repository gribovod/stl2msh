#include "pch.h"
#include <cstdint>
#include "PointF.h"
#include <cmath>

PointF PointF::operator-(const PointF& v) const {
	return PointF(x - v.x, y - v.y, z - v.z);
}

PointF PointF::operator+(const PointF& v) const {
	return PointF(x + v.x, y + v.y, z + v.z);
}

PointF PointF::operator%(const PointF& v) const {
	return PointF(y*v.z - z * v.y, z*v.x - x * v.z, x*v.y - y * v.x);
}

PointF PointF::operator*(const float v) const
{
	return PointF(x*v, y*v, z*v);
}

void PointF::operator*=(const float v)
{
	x *= v;
	y *= v;
	z *= v;
}

float PointF::operator*(const PointF& v) const {
	return x * v.x + y * v.y + z * v.z;
}

float PointF::module() const
{
	return sqrtf(x*x + y * y + z * z);
}

PointF::PointF(FILE* source)
{
	auto rCount = 0;
	rCount = fread(&x, 4, 1, source);
	rCount = fread(&y, 4, 1, source);
	rCount = fread(&z, 4, 1, source);
}

PointF::PointF()
{
	x = 0;
	y = 0;
	z = 0;
}

PointF::PointF(float X, float Y, float Z)
	:x(X), y(Y), z(Z)
{
}

PointF::~PointF()
{
}
