#include "pch.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "OrbiterMesh.h"
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

OrbiterMeshGroup & OrbiterMesh::addGroup(std::string groupName)
{
	groups_.push_back(OrbiterMeshGroup(groupName));
	return groups_.back();
}

void OrbiterMesh::save()
{
	ofstream outf(path_);
	outf << std::fixed << std::setprecision(8);
	if (!outf)
	{
		cerr << "File " << path_ << " is not open to write" << endl;
		return;
	}
	outf << "MSHX1" << endl;
	outf << "GROUPS " << groups_.size() << endl;
	for (OrbiterMeshGroup& g : groups_) {
		g.saveToFile(outf);
	}
	outf << "MATERIALS 0" << endl;
	outf << "TEXTURES 0" << endl;
	outf.close();
}

void OrbiterMesh::load()
{
	ifstream inf(path_);

	inf.close();
}

void OrbiterMesh::Flip(PointF::Axis axis)
{
	for (OrbiterMeshGroup& g : groups_) {
		g.Flip(axis);
	}
}

void OrbiterMesh::Scale(float scale)
{
	for (OrbiterMeshGroup& g : groups_) {
		g.Scale(scale);
	}
}

void OrbiterMesh::Rotate(PointF::Axis axis, float angle)
{
	for (OrbiterMeshGroup& g : groups_) {
		g.Rotate(axis, cosf(angle * M_PI / 180), sinf(angle * M_PI / 180));
	}
}

void OrbiterMesh::Rotate(PointF::Axis axis, float cosA, float sinA)
{
	for (OrbiterMeshGroup& g : groups_) {
		g.Rotate(axis, cosA, sinA);
	}
}

OrbiterMesh::OrbiterMesh(char* filePath)
	:path_(filePath)
{
}


OrbiterMesh::~OrbiterMesh()
{
}
