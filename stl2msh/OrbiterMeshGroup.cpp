#include "pch.h"
#include <iostream>
#include "OrbiterMeshGroup.h"

using namespace std;

void OrbiterMeshGroup::addTriangle(PointF point1, PointF point2, PointF point3, PointF normal)
{
	int p1 = addPoint(point1, normal);
	int p2 = addPoint(point2, normal);
	int p3 = addPoint(point3, normal);
	triangles_.push_back(OrbiterMeshTriangle(points_, p1, p2, p3));
}

int OrbiterMeshGroup::addPoint(PointF point, PointF normal) {
	int nearPointIndex = findNearPoint(point, NearDistance);
	if (nearPointIndex >= 0) {
		return nearPointIndex;
	}
	else {
		points_.push_back(OrbiterMeshPoint(point, normal));
		return points_.size() - 1;
	}
}

int OrbiterMeshGroup::findNearPoint(PointF point, float NearDistance) {
	return -1;
}

void OrbiterMeshGroup::Flip(PointF::Axis axis)
{
	for (auto p = points_.begin(); p < points_.end(); ++p) {
		p->Flip(axis);
	}
	for (auto t = triangles_.begin(); t < triangles_.end(); ++t) {
		t->Flip(axis);
	}
}

void OrbiterMeshGroup::Scale(float scale)
{
	for (auto p = points_.begin(); p < points_.end(); ++p) {
		p->Scale(scale);
	}
}

void OrbiterMeshGroup::Rotate(PointF::Axis axis, float cosA, float sinA)
{
	for (auto p = points_.begin(); p < points_.end(); ++p) {
		p->Rotate(axis, cosA, sinA);
	}
}

void OrbiterMeshGroup::saveToFile(ofstream & stream)
{
	if (name_.size() > 0) {
		stream << "LABEL " << name_ << endl;
	}
	if (noNormal_) {
		stream << "NONORMAL" << endl;
	}
	stream << "MATERIAL 0" << endl;
	stream << "GEOM " << points_.size() << " " << triangles_.size() << endl;
	for (auto p : points_) {
		p.saveToFile(stream);
	}
	for (auto t : triangles_) {
		t.saveToFile(stream);
	}
}

OrbiterMeshGroup::OrbiterMeshGroup(string name)
	: name_(name)
	, noNormal_(false)
{

}

OrbiterMeshGroup::~OrbiterMeshGroup()
{
}
