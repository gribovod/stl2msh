#include "pch.h"
#include <iostream>
#include <fcntl.h>
#include "STLFile.h"

using namespace std;

vector<STLTriangle> STLFile::triangles()
{
	return triangles_;
}

STLFile::State STLFile::getState()
{
	return state_;
}

string STLFile::name()
{
	return *name_;
}

STLFile::STLFile(char* filePath)
	: file_(filePath)
	, state_(State::Unknown)
{
	auto fId = fopen(filePath, "rb");
	if (!fId) {
		cerr << "File " << filePath << " is not open" << endl;
		state_ = State::NotExists;
		return;
	}
	char buffer[7]{};
	if (fread(buffer, 1, 6, fId) < 6) {
		cerr << "File " << filePath << " is wrong" << endl;
		state_ = State::WrongFile;
		return;
	}
	fclose(fId);
	if (strcmp(buffer, "solid ") == 0) {
		loadASCII();
	}
	else {
		loadBinary();
	}
}


STLFile::~STLFile()
{
}

void STLFile::loadASCII()
{
	ifstream source(file_);
	string tmpLine;
	source >> tmpLine; // Read "solid"
	source >> tmpLine;
	name_ = new string(tmpLine);
	source.close();
}

void STLFile::loadBinary()
{
	auto pointPos = file_.find_last_of(".");
	if (pointPos == string::npos) {
		pointPos = file_.size() - 1;
	}
	auto dividerPos = file_.find_last_of("\\/", pointPos);
	if (dividerPos == string::npos) {
		dividerPos = 0;
	}
	name_ = new string(file_.substr(dividerPos + 1, pointPos - dividerPos - 1));

	auto fId = fopen(file_.c_str(), "rb");
	if (fseek(fId, 80, SEEK_SET) != 0) {
		cerr << "File " << file_ << " is wrong" << endl;
		state_ = State::WrongFile;
		return;
	}
	uint32_t triangle_count = 0;
	fread(&triangle_count, 4, 1, fId);
	triangles_.reserve(triangle_count);
	for (int i = 0; i < triangle_count; ++i) {
		triangles_.push_back(STLTriangle(fId));
	}
	state_ = State::Loaded;
}
