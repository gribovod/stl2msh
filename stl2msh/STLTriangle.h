#pragma once
#include <cstdint>
#include <stdio.h>
#include "PointF.h"

class STLTriangle
{
public:
	PointF normal;
	PointF points[3];
	uint16_t attr;
	STLTriangle(FILE* source);
	~STLTriangle();
};

