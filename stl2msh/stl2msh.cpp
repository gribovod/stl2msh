#include "pch.h"
#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>
#include <sys/stat.h>
#include "OrbiterMesh.h"
#include "STLFile.h"
#include "STLTriangle.h"

using namespace std;

void usage() {
	cout << "Usage: stl2msh [options] stl_path [msh_path]" << endl;
	cout << "	stl_path - path to existing STL file" << endl;
	cout << "	msh_path - if not set, use stl_path with \"msh\" extension" << endl;
	cout << "	Options:" << endl;
	cout << "	-f N	flip by N axis (X, Y or Z)" << endl;
	cout << "	-r N G	rotate by N axis on G degrees" << endl;
	cout << "	-s S	scale on S" << endl;
}

void convert(STLFile& stl, OrbiterMesh& orbiter) {
	OrbiterMeshGroup& group = orbiter.addGroup(stl.name());
	for (STLTriangle& t : stl.triangles()) {
		group.addTriangle(t.points[0], t.points[1], t.points[2], t.normal);
	}
}

char* getOrbiterFileName(char* stlFileName) {
	string file(stlFileName);
	auto pointPos = file.find_last_of(".");
	if (pointPos == string::npos) {
		pointPos = file.size() - 1;
	}
	file = string(file.substr(0, pointPos + 1)) + "msh";
	char* result = new char[file.size() + 1]{};
	file.copy(result, file.size());
	return result;
}

int main(int argc, char** argv)
{
    cout << "STL to Orbiter Mesh File Converter" << endl; 
	if (argc < 2) {
		usage();
		return 1;
	}
	struct stat s;
	if (stat(argv[1], &s) != 0) {
		usage();
		return 1;
	}

	STLFile stl(argv[1]);
	if (stl.getState() != STLFile::State::Loaded) {
		return 1;
	}
	char* orbiterFileName = argc > 2 ? argv[2] : getOrbiterFileName(argv[1]);
	OrbiterMesh orbiterMesh(orbiterFileName);
	if (stat(orbiterFileName, &s) == 0) {
		orbiterMesh.load();
	} 
	convert(stl, orbiterMesh);
	orbiterMesh.Flip(PointF::Axis::Y);
	orbiterMesh.Rotate(PointF::Axis::X, 90.0);
	orbiterMesh.Scale(0.001);
	orbiterMesh.save();
}
