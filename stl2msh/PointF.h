#pragma once
#include <stdio.h>

class PointF
{
public:
	enum class Axis {
		X = 1,
		Y,
		Z
	};

	PointF operator-(const PointF& v) const;
	PointF operator+(const PointF& v) const;
	PointF operator%(const PointF& v) const;
	PointF operator*(const float v) const;
	void operator*=(const float v);
	float operator*(const PointF& v) const;
	float module() const;
	float x;
	float y;
	float z;
	PointF();
	PointF(FILE* source);
	PointF(float X, float Y, float Z);
	~PointF();
};


