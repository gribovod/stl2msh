#pragma once

#include <fstream>
#include <vector>
#include "PointF.h"
#include "OrbiterMeshPoint.h"

using namespace std;

class OrbiterMeshTriangle
{
public:
	void Flip(PointF::Axis axis);
	void saveToFile(std::ofstream& stream);
	OrbiterMeshTriangle(vector<OrbiterMeshPoint> points, int p1, int p2, int p3);
	~OrbiterMeshTriangle();
private:
	int p1_;
	int p2_;
	int p3_;
};

