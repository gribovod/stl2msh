#pragma once

#include <fstream>
#include <string>
#include <vector>
#include "PointF.h"
#include "OrbiterMeshTriangle.h"
#include "OrbiterMeshPoint.h"

using namespace std;

class OrbiterMeshGroup
{
public:
	static constexpr float NearDistance = 1.0;
	void addTriangle(PointF point1, PointF point2, PointF point3, PointF normal);
	void Flip(PointF::Axis axis);
	void Scale(float scale);
	void Rotate(PointF::Axis axis, float cosA, float sinA);
	void saveToFile(ofstream& stream);
	OrbiterMeshGroup(string name);
	~OrbiterMeshGroup();
private:
	int addPoint(PointF point, PointF normal);
	int findNearPoint(PointF point, float NearDistance);
	string name_;
	vector<OrbiterMeshPoint> points_;
	vector<OrbiterMeshTriangle> triangles_;
	bool noNormal_;
};
