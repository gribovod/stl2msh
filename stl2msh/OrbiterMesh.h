#pragma once

#include <string>
#include <vector>
#include "OrbiterMeshGroup.h"

class OrbiterMesh
{
public:
	OrbiterMeshGroup& addGroup(std::string groupName);
	void save();
	void load();
	void Flip(PointF::Axis axis);
	void Scale(float scale);
	void Rotate(PointF::Axis axis, float angle);
	void Rotate(PointF::Axis axis, float cosA, float sinA);
	OrbiterMesh(char* filePath);
	~OrbiterMesh();
private:
	std::vector<OrbiterMeshGroup> groups_;
	std::string path_;
};

