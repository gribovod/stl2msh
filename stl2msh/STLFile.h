#pragma once
#include <string>
#include <fstream>
#include <vector>
#include "STLTriangle.h"

class STLFile
{
public:
	enum class State
	{
		Unknown,
		NotExists,
		WrongFile,
		Loaded
	};
	State getState();
	std::string name();
	std::vector<STLTriangle> triangles();
	STLFile(char* filePath);
	~STLFile();
private:
	void loadASCII();
	void loadBinary();
	std::string file_;
	std::string* name_;
	std::vector<STLTriangle> triangles_;
	State state_;
};

