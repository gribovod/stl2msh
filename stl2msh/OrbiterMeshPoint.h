#pragma once
#include <fstream>
#include "PointF.h"

class OrbiterMeshPoint
{
public:
	void Flip(PointF::Axis axis);
	void Scale(float scale);
	void Rotate(PointF::Axis axis, float cosA, float sinA);
	void saveToFile(std::ofstream& stream,  bool noNormal = false);
	OrbiterMeshPoint(PointF coord, PointF normal);
	~OrbiterMeshPoint();
	const PointF coord() const { return coord_; }
	const PointF normal() const { return normal_; }
private:
	PointF coord_;
	PointF normal_;
};

